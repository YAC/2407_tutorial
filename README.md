# YAC Tutorial

This is a guided tutorial for the usage of Yet Another Coupler [YAC](https://dkrz-sw.gitlab-pages.dkrz.de/yac/).

## Instructions

- check out the `main` branch of this repository:

    `git clone --depth=1 -b main`

- log into [Jupyterhub @ DKRZ](https://jupyterhub.dkrz.de/hub/login?next=%2Fhub%2Fhome)
- work through Jupyter Notebooks `Task_0.ipynb` to `Task_4.ipynb`
